/**
 * Created by No One on 4/30/2017.
 */
var mongoose = require("mongoose");
var userSchema = mongoose.Schema({
    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    }
});
var User = module.exports= mongoose.model("User",userSchema);
module.exports.getUsers = function (callback,limit) {
    User.find(callback).limit(limit);
};
module.exports.addUser = function (user,callback) {
    User.create(user,callback);
};
module.exports.updateUser = function (id,user,options,callback) {
    var query= {_id:id};
    var update = {
       username:user.username,
        password:user.password
    };
    User.findOneAndUpdate(query,update,options,callback);
};
module.exports.deleteUser = function (id,callback) {
    var query = {_id:id};
    User.remove(query,callback);
};
module.exports.getUserById = function (id,callback) {
    User.findById(id,callback);
};