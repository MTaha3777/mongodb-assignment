var express = require('express');
var router = express.Router();
var User = require("../models/users")
/* GET users listing. */
router.get('/', function(req, res, next) {
    User.getUsers(function (err,user) {
        if(err){
            throw err;
        }
        res.json(user);

    });

// res.send("hello world")

});
router.post('/', function(req, res, next) {
    var user = req.body;
    User.addUser(user,function (err,user) {
        if(err){
            throw err;
        }
        res.json(user);

    })


});
router.put('/:_id', function(req, res, next) {
    var id = req.params._id;
    var user = req.body;
    User.updateUser(id,user,{},function (err,user) {
        if(err){
            throw err;
        }
        res.json(user);

    })


});
router.delete('/:_id', function(req, res, next) {
    var id = req.params._id;

    User.deleteUser(id,function (err,user) {
        if(err){
            throw err;
        }
        res.json(user);

    })


});
router.get('/:_id', function(req, res, next) {
    var id = req.params._id;
    User.getUserById(id,function (err,user) {
        if(err){
            throw err;
        }
        res.json(user);

    })


});




module.exports = router;
